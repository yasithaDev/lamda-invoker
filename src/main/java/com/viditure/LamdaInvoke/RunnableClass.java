package com.viditure.LamdaInvoke;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RunnableClass 
{
	//RunnableClass 

	private static LamdaInvoker lamdaInvoker;
    @Autowired
	public void setLamdaInvoker(LamdaInvoker object){
		RunnableClass.lamdaInvoker = object;
	}
    
	private static JsonPayLoadPreparer preparer;
    @Autowired
	public void setPreparer(JsonPayLoadPreparer preparer){
		RunnableClass.preparer = preparer;
	}

	public static void main(String[] args) 
	{
		SpringApplication.run(RunnableClass.class, args);

		String functionName="videoTranscode";
		
		try
		{
			String payload = preparer.preparerForConvert("local.imal.lamda.ffmpeg","before.mov","mov2mp4Converted.mp4","mov2Mp4Command",4);
			lamdaInvoker.runWithPayload(functionName,payload);
			
			payload = preparer.preparerForConvert("local.imal.lamda.ffmpeg","before.flv","flv2mp4Converted.mp4","flv2Mp4Command",2);
			lamdaInvoker.runWithPayload(functionName,payload);
			
			payload = preparer.preparerForConvert("local.imal.lamda.ffmpeg","img.jpg","jpg2pngConverted.png","jpgJPFGgifToPngCommand",2);
			lamdaInvoker.runWithPayload(functionName,payload);
			
			payload = preparer.preparerForConvert("local.imal.lamda.ffmpeg","before.webp","webp2pngConverted.png","webpTopngCommand",2);
			lamdaInvoker.runWithPayload(functionName,payload);
			
			payload = preparer.preparerForMearge("local.imal.lamda.ffmpeg","before.webm","before.wav","webmAndwavMergeConverted.mp4","webmAndwavMergeCommand",2);
			lamdaInvoker.runWithPayload(functionName,payload);
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
		
	}
}
