package com.viditure.LamdaInvoke;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
@Component
public class JsonPayLoadPreparer 
{
	public String preparerForConvert(String bucket_name,String beforeName,String convertedName,String type,int trancepose) throws JSONException
	{
		String jsonString="";
		JSONObject jObjLevel_0 = new JSONObject();
		JSONObject jObjLevel_1 = new JSONObject();
		JSONObject jObjLevel_2=new JSONObject();
		JSONArray jArrLevel_1 = new JSONArray();
		
				jObjLevel_2.put("bucket_name",bucket_name);
				jObjLevel_2.put("object_key",beforeName);
			jArrLevel_1.put(jObjLevel_2);
		jObjLevel_0.put("download",jArrLevel_1);

		jObjLevel_1 = new JSONObject();
		jObjLevel_2=new JSONObject();
		jArrLevel_1 = new JSONArray();

			jObjLevel_1.put("bucket_name", bucket_name);
			jObjLevel_1.put("object_key",convertedName);
		jObjLevel_0.put("upload",jObjLevel_1);

		jObjLevel_1 = new JSONObject();
		jObjLevel_2=new JSONObject();
		jArrLevel_1 = new JSONArray();

			jObjLevel_1.put("type", type);
			jObjLevel_1.put("transpose",trancepose);
		jObjLevel_0.put("conversion",jObjLevel_1);
		
		jsonString=jObjLevel_0.toString();
		return jsonString;
	}

	public String preparerForMearge(String bucket_name,String beforeName1,String beforeName2,String convertedName,String type,int trancepose) throws JSONException
	{
		String jsonString="";
		JSONObject jObjLevel_0 = new JSONObject();
		JSONObject jObjLevel_1 = new JSONObject();
		JSONObject jObjLevel_2=new JSONObject();
		JSONArray jArrLevel_1 = new JSONArray();
		
				jObjLevel_2.put("bucket_name",bucket_name);
				jObjLevel_2.put("object_key",beforeName1);
			jArrLevel_1.put(jObjLevel_2);
			
			jObjLevel_2=new JSONObject();
			
				jObjLevel_2.put("bucket_name",bucket_name);
				jObjLevel_2.put("object_key",beforeName2);
			jArrLevel_1.put(jObjLevel_2);
			
		jObjLevel_0.put("download",jArrLevel_1);

		jObjLevel_1 = new JSONObject();
		jObjLevel_2=new JSONObject();
		jArrLevel_1 = new JSONArray();

			jObjLevel_1.put("bucket_name", bucket_name);
			jObjLevel_1.put("object_key",convertedName);
		jObjLevel_0.put("upload",jObjLevel_1);

		jObjLevel_1 = new JSONObject();
		jObjLevel_2=new JSONObject();
		jArrLevel_1 = new JSONArray();

			jObjLevel_1.put("type", type);
			jObjLevel_1.put("transpose",trancepose);
		jObjLevel_0.put("conversion",jObjLevel_1);
		
		jsonString=jObjLevel_0.toString();
		return jsonString;
	}
}
