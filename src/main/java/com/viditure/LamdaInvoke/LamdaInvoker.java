package com.viditure.LamdaInvoke;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaAsync;
import com.amazonaws.services.lambda.AWSLambdaAsyncClient;
import com.amazonaws.services.lambda.AWSLambdaAsyncClientBuilder;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;

@Component
public class LamdaInvoker 
{	
	@Value("${cloud.aws.region.static}")
	private String region;
	//public String region="us-west-1";
	
	@Value("${aws.accessKeyId}")
	private String awsAccessKeyId;
	//String awsAccessKeyId="AKIAID4RHJAG7YH3SWIA";
	
	@Value("${aws.secretKey}")
	private String awsSecretKey;
	//String awsSecretKey="+NwfHAOzaA04OeESmQCV2x5UIl8dPvy3sRd9tpOd";
	 
    public void runWithPayload(String functionName, String payload)
    { 
    	AWSCredentials credentials = new BasicAWSCredentials(this.awsAccessKeyId,this.awsSecretKey);
    	AWSCredentialsProvider credProvider =new AWSStaticCredentialsProvider(credentials);
    	AWSLambdaAsyncClientBuilder clientBuilder =AWSLambdaAsyncClientBuilder.standard();
    	clientBuilder.withCredentials(credProvider);
    	clientBuilder.withRegion(Regions.fromName(this.region));
    	AWSLambdaAsync client= clientBuilder.build();
        InvokeRequest request = new InvokeRequest();
        request.withFunctionName(functionName).withPayload(payload);
        InvokeResult invoke = client.invoke(request);
        System.out.println("Result invoking " + functionName + ":" + invoke);
    }
}
